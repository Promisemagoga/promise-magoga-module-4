import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

import 'Login.dart';

 void main() {
   runApp(MyApp());
    }


class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Department of health library',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.pink
        ),
              ),
      home: AnimatedSplashScreen(
        splash: 'images/doctor-examining-patient-clinic-illustrated_23-2148856559.jpg',
        nextScreen: Login(),
        splashTransition: SplashTransition.rotationTransition,
        duration: 3000,
        backgroundColor: Colors.pink,
      )
    );
  }
}


